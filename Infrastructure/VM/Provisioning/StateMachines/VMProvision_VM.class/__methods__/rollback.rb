#
# Description: call retirement state machine to a specific step
# set inputs valures name: rollback_namespec, rollback_class_nmae, rollback_instance_name and rollback_state
#


next_state=$evm.inputs["rollback_state"]

options={}
options [:namespace]=$evm.inputs["rollback_namespace"]||'Infrastructure'
options[:class_name]=$evm.inputs["rollback_class_name"]||'VM/Retirement/StateMachines/VMRetirement'
options[:instance_name]=$evm.inputs["rollback_instance_name"]||'Default'
options[:message]= 'create'
options[:user_id]= $evm.root['user'].id

options[:attrs]={}

#attrs["miq_zone"]= 'Default Zone'
#attrs[:user_id]= $evm.root['user'].id
options[:attrs]["rollback_state"]=next_state
options[:attrs]['Vm::vm']=$evm.root["vm"] rescue nil
#attrs['Service::service']=$evm.root["service"].id rescue nil
$evm.log(:info," Rollback options=#{options}")
result=$evm.execute("create_automation_request",options,"admin",true)
