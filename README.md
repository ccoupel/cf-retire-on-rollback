# cf-rollback

This domain is us to be ised to handle rollback.
the purpose is, when a provisionning step is in error state, to call the retirement state machine starting to a prticular step.
The focus has to e made on that the retirement steps are in the opposit order of the provisionning

the rollback can be launched from any state of the provisionning sate machine by setting the on_error value  to:
 => **/System/Request.bad_RollBack (rollback_state => "ReleaseIPAddress") **
 or
 =>  **/System/Request.bad_RollBack (rollback_state => "ReleaseIPAddress", rollback_namespace => "Infrastructure", rollback_class_name => "VM/Retirement/StateMachines/VMRetirement", rollback_instance_name => "RollBack") **
 
 the first state of the retirement state machine MUST be set to call  **/Infrastructure/VM/Retirement/StateMachines/Methods/rollback_jumper **